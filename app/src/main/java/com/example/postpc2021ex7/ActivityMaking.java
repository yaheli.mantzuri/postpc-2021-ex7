package com.example.postpc2021ex7;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.Objects;

public class ActivityMaking extends AppCompatActivity {
    public HolderNew holder = null;
    String ID;

    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_making);


        if (holder == null) {
            holder =  orderApplication.getInstance().getHolder();
        }

        Intent intentOpenedMe = getIntent();
        ID = intentOpenedMe.getStringExtra("ID");

        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();


        final DocumentReference docRef = firebaseFirestore.collection("orders").document(ID);
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("a", "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    Log.d("a", "Current data: " + snapshot.getData());
                    if(Objects.equals(Objects.requireNonNull(snapshot.getData()).get("status"), "ready")){
                        holder.downloadDocunment(ID);
                        Intent intentToOpenService = new Intent(ActivityMaking.this, ActivityReady.class);
                        intentToOpenService.putExtra("ID", ID);
                        startActivity(intentToOpenService);
                        finish();
                    }

                } else {
                    Log.d("a", "Current data: null");
                }
            }
        });

    }
}
