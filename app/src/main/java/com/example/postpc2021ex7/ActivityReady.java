package com.example.postpc2021ex7;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.firestore.FirebaseFirestore;

public class ActivityReady extends AppCompatActivity {
    public HolderNew holder = null;
    String ID;

    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ready);


        Button button = findViewById(R.id.button);

        if (holder == null) {
            holder =  orderApplication.getInstance().getHolder();
        }

        Intent intentOpenedMe = getIntent();
        ID = intentOpenedMe.getStringExtra("ID");


        button.setOnClickListener(v -> {
            holder.DeleteDocument("orders", ID);
            Intent intentToOpenService = new Intent(ActivityReady.this, ActivityNewOrder.class);
            startActivity(intentToOpenService);
            finish();
        });

    }
}
