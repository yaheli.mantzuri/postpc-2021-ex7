package com.example.postpc2021ex7;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.io.Serializable;
import java.util.Objects;


public class ActivityEditOrder extends AppCompatActivity {
    String ID;
    String customer_name;
    int pickles;
    boolean hummus;
    boolean tahini;
    String comment;
    String status;
    public HolderNew holder = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_your_order);


        if (holder == null) {
            holder =  orderApplication.getInstance().getHolder();
        }

        Intent intentOpenedMe = getIntent();
        ID = intentOpenedMe.getStringExtra("ID");
        customer_name = intentOpenedMe.getStringExtra("customer_name");
        pickles = intentOpenedMe.getIntExtra("pickles", 0);
        hummus = intentOpenedMe.getBooleanExtra("hummus",false);
        tahini = intentOpenedMe.getBooleanExtra("tahini",false);
        comment = intentOpenedMe.getStringExtra("comment");
        status = intentOpenedMe.getStringExtra("status");




        EditText edit_name = findViewById(R.id.edit_name);
        EditText edit_comment = findViewById(R.id.edit_comment);
        EditText edit_pickles = findViewById(R.id.edit_pickles);
        Button button_save = findViewById(R.id.button_save);
        Button button_delete = findViewById(R.id.button_delete);
        CheckBox checkBox_hummus = findViewById(R.id.checkBox_hummus);
        CheckBox checkBox_tahini = findViewById(R.id.checkBox_tahini);
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();



        final DocumentReference docRef = firebaseFirestore.collection("orders").document(ID);
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("a", "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    Log.d("a", "Current data: " + snapshot.getData());
                    if(Objects.equals(Objects.requireNonNull(snapshot.getData()).get("status"), "in-progress")){

                        holder.downloadDocunment(ID);
                        Intent intentToOpenService = new Intent(ActivityEditOrder.this, ActivityMaking.class);
                        intentToOpenService.putExtra("ID", ID);
                        startActivity(intentToOpenService);
                        finish();
                    }
                } else {
                    Log.d("a", "Current data: null");
                }
            }
        });


        checkBox_hummus.setChecked(hummus);

        checkBox_tahini.setChecked(tahini);



        edit_name.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            public void afterTextChanged(Editable s) {
                customer_name = edit_name.getText().toString();

            }
        });

        edit_comment.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            public void afterTextChanged(Editable s) {
                comment = edit_comment.getText().toString();
            }
        });


        edit_pickles.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            public void afterTextChanged(Editable s) {
                String p = edit_pickles.getText().toString();
                if (isNumeric(p)){
                    pickles = Integer.parseInt(p);
                    button_save.setEnabled(true);
                } else {
                    button_save.setEnabled(false);
                }
            }
        });

        checkBox_hummus.setOnClickListener(v -> {
            if (checkBox_hummus.isChecked()) {
                checkBox_hummus.setChecked(true);
                hummus = true;
            } else {
                checkBox_hummus.setChecked(false);
                hummus = false;
            }
        });

        checkBox_tahini.setOnClickListener(v -> {
            if (checkBox_tahini.isChecked()) {
                checkBox_tahini.setChecked(true);
                tahini = true;
            } else {
                checkBox_tahini.setChecked(false);
                tahini = false;
            }
        });

        button_save.setOnClickListener(v -> {
            holder.updateDocument(ID, customer_name, pickles, hummus, tahini, comment, status);
        });

        button_delete.setOnClickListener(v -> {
            holder.DeleteDocument("orders", ID);
            Intent intentToOpenService = new Intent(ActivityEditOrder.this, ActivityNewOrder.class);
            startActivity(intentToOpenService);
            finish();
        });

    }

    public static boolean isNumeric(String strNum) {
        double d;
        if (strNum == null) {
            return false;
        }
        try {
            d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return d >= 0 && d <= 10;
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {


        super.onSaveInstanceState(outState);
        outState.putString("ID", ID);
        outState.putString("customer_name", customer_name);
        outState.putInt("pickles", pickles);

        outState.putBoolean("hummus",hummus);
        outState.putBoolean("tahini",tahini);

        outState.putString("comment",comment);
        outState.putString("status",status);



    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        ID = savedInstanceState.getString("ID");
        customer_name = savedInstanceState.getString("customer_name");
        pickles = savedInstanceState.getInt("pickles");
        hummus = savedInstanceState.getBoolean("hummus");
        tahini = savedInstanceState.getBoolean("tahini");
        comment = savedInstanceState.getString("comment");
        status = savedInstanceState.getString("saveState");

    }
}
