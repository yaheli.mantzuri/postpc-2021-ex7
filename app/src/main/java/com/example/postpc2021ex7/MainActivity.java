package com.example.postpc2021ex7;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity  extends AppCompatActivity {

    public HolderNew holder = null;

    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);


        if (holder == null) {
            holder =  orderApplication.getInstance().getHolder();
        }
        if (holder.fireStoreSandwichOrderSave != null){
            switch (holder.fireStoreSandwichOrderSave.status) {
                case "waiting": {
                    Intent intentToOpenService = new Intent(MainActivity.this, ActivityEditOrder.class);
                    intentToOpenService.putExtra("ID", holder.fireStoreSandwichOrderSave.id);
                    intentToOpenService.putExtra("customer_name", holder.fireStoreSandwichOrderSave.customer_name);
                    intentToOpenService.putExtra("pickles", holder.fireStoreSandwichOrderSave.pickles);
                    intentToOpenService.putExtra("hummus", holder.fireStoreSandwichOrderSave.hummus);
                    intentToOpenService.putExtra("tahini", holder.fireStoreSandwichOrderSave.tahini);
                    intentToOpenService.putExtra("comment", holder.fireStoreSandwichOrderSave.comment);
                    intentToOpenService.putExtra("status", holder.fireStoreSandwichOrderSave.status);
                    startActivity(intentToOpenService);
                    finish();
                    break;
                }
                case "in-progress": {
                    Intent intentToOpenService = new Intent(MainActivity.this, ActivityMaking.class);
                    intentToOpenService.putExtra("ID", holder.fireStoreSandwichOrderSave.id);
                    startActivity(intentToOpenService);
                    finish();
                    break;
                }
                case "ready": {
                    Intent intentToOpenService = new Intent(MainActivity.this, ActivityReady.class);
                    intentToOpenService.putExtra("ID", holder.fireStoreSandwichOrderSave.id);
                    startActivity(intentToOpenService);
                    finish();
                    break;
                }
            }

        } else {
            Intent intent = new Intent(MainActivity.this, ActivityNewOrder.class);
            startActivity(intent);
            finish();
        }

    }
}
