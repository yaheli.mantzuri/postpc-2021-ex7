package com.example.postpc2021ex7;

import android.content.Context;

public interface HolderNewInterface {



    void initializeFromSpName();

    void initializeFromSp();

    public FireStoreSandwichOrder getOrderFromString(String str);


    public String getStringfromToDo(FireStoreSandwichOrder fireStoreSandwichOrder);

    public String AddDocument(String customer_name, int pickles, boolean hummus, boolean tahini, String comment, String status);


    public void updateDocument(String ID, String customer_name, int pickles, boolean hummus, boolean tahini, String comment, String status);


    public void DeleteDocument(String collection, String ID);

    public void downloadDocunment(String ID);
}
