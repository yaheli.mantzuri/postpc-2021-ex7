package com.example.postpc2021ex7;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

public class HolderNew implements HolderNewInterface{

    FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    private final Context context;

    private final SharedPreferences sp;
    public FireStoreSandwichOrder fireStoreSandwichOrder = null;
    public FireStoreSandwichOrder fireStoreSandwichOrderSave = null;
    public String name = null;

    public HolderNew(Context context) {
        this.context = context;
        this.sp = context.getSharedPreferences("db_order_new_3", Context.MODE_PRIVATE);
        initializeFromSp();
        initializeFromSpName();


    }

    public void initializeFromSpName() {
        Set<String> keys = sp.getAll().keySet();
        for (String key : keys) {
            if (key.equals("name")){
                name = sp.getString(key, null);
            }

        }
    }

    public void initializeFromSp() {
        Set<String> keys = sp.getAll().keySet();
        for (String key : keys) {
            if(!key.equals("name")){
                String save = sp.getString(key, null);
                FireStoreSandwichOrder order = getOrderFromString(save);
                if (!order.status.equals("done")) {
                    if (order != null) {
                        fireStoreSandwichOrderSave = order;
                    }
                }
            }

        }
    }

    public FireStoreSandwichOrder getOrderFromString(String str) {
        if (str == null) {
            return null;
        }

        String[] arrOfStr1 = str.split("#", 7);
        FireStoreSandwichOrder order = new FireStoreSandwichOrder();
        order.id = arrOfStr1[0];
        order.customer_name = arrOfStr1[1];
        order.pickles = Integer.parseInt(arrOfStr1[2]);
        order.hummus = arrOfStr1[3].equals("true");
        order.tahini = arrOfStr1[4].equals("true");
        order.comment = arrOfStr1[5];
        order.status = arrOfStr1[6];

        return order;
    }


    public String getStringfromToDo(FireStoreSandwichOrder fireStoreSandwichOrder) {
        return fireStoreSandwichOrder.id + "#" + fireStoreSandwichOrder.customer_name + "#" +
                fireStoreSandwichOrder.pickles + "#" + fireStoreSandwichOrder.hummus + "#" +
                fireStoreSandwichOrder.tahini + "#" + fireStoreSandwichOrder.comment + "#" + fireStoreSandwichOrder.status;
    }

    public String AddDocument(String customer_name, int pickles, boolean hummus, boolean tahini, String comment, String status) {
        String newID = UUID.randomUUID().toString();
        FireStoreSandwichOrder order = new FireStoreSandwichOrder(newID, customer_name, pickles, hummus, tahini, comment, status);
        firebaseFirestore.collection("orders").document(newID).set(order);

        SharedPreferences.Editor editor_name = sp.edit();
        editor_name.putString("name", order.customer_name);
        editor_name.apply();


        SharedPreferences.Editor editor = sp.edit();
        editor.putString(order.id, getStringfromToDo(order));
        editor.apply();

        return newID;

    }

    public void updateDocument(String ID, String customer_name, int pickles, boolean hummus, boolean tahini, String comment, String status) {
        FireStoreSandwichOrder order = new FireStoreSandwichOrder(ID, customer_name, pickles, hummus, tahini, comment, status);
        firebaseFirestore.collection("orders").document(ID).set(order);


        SharedPreferences.Editor editor = sp.edit();
        editor.putString(order.id, getStringfromToDo(order));
        editor.apply();


    }

    public void DeleteDocument(String collection, String ID) {
        firebaseFirestore.collection(collection).document(ID).delete();
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(ID);
        editor.apply();
    }


    public void downloadDocunment(String ID) {
        firebaseFirestore.collection("orders").document(ID).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        fireStoreSandwichOrder = documentSnapshot.toObject(FireStoreSandwichOrder.class);

                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString(fireStoreSandwichOrder.id, getStringfromToDo(fireStoreSandwichOrder));
                        editor.apply();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }
}




class FireStoreSandwichOrder implements Serializable{
    public String id = "";
    public String customer_name = "";
    public int pickles = 0;
    public boolean hummus = false;
    public boolean tahini = false;
    public String comment = "";
    public String status = "";

    public FireStoreSandwichOrder(String newID, String customer_name, int pickles, boolean hummus, boolean tahini, String comment, String status) {
        this.id = newID;
        this.customer_name = customer_name;
        this.pickles = pickles;
        this.hummus = hummus;
        this.tahini = tahini;
        this.comment = comment;
        this.status = status;

    }

    public FireStoreSandwichOrder() {
    }
}
