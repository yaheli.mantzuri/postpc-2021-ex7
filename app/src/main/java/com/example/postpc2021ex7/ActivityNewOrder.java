package com.example.postpc2021ex7;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ActivityNewOrder extends AppCompatActivity {
    String customer_name;
    int pickles;
    boolean hummus;
    boolean tahini;
    String comment;
    String status = "waiting";
    public HolderNew holder = null;

    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order);

        if (holder == null) {
            holder =  orderApplication.getInstance().getHolder();
        }



        EditText edit_name = findViewById(R.id.edit_name);
        EditText edit_comment = findViewById(R.id.edit_comment);
        EditText edit_pickles = findViewById(R.id.edit_pickles);
        Button button_confirm = findViewById(R.id.button_confirm);
        CheckBox checkBox_hummus = findViewById(R.id.checkBox_hummus);
        CheckBox checkBox_tahini = findViewById(R.id.checkBox_tahini);



        if (holder.name == null) {
            edit_name.addTextChangedListener(new TextWatcher() {
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                public void afterTextChanged(Editable s) {
                    customer_name = edit_name.getText().toString();

                }
            });
        } else {
            customer_name = holder.name;
        }

        edit_comment.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            public void afterTextChanged(Editable s) {
                comment = edit_comment.getText().toString();
            }
        });

        edit_pickles.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            public void afterTextChanged(Editable s) {
                String p = edit_pickles.getText().toString();
                if (isNumeric(p)){
                    pickles = Integer.parseInt(p);
                    button_confirm.setEnabled(true);
                } else {
                    button_confirm.setEnabled(false);
                }
            }
        });

        checkBox_hummus.setOnClickListener(v -> {
            if (checkBox_hummus.isChecked()) {
                checkBox_hummus.setChecked(true);
                hummus = true;
            } else {
                checkBox_hummus.setChecked(false);
                hummus = false;
            }
        });

        checkBox_tahini.setOnClickListener(v -> {
            if (checkBox_tahini.isChecked()) {
                checkBox_tahini.setChecked(true);
                tahini = true;
            } else {
                checkBox_tahini.setChecked(false);
                tahini = false;
            }
        });

        button_confirm.setOnClickListener(v -> {
            String id = holder.AddDocument(customer_name, pickles, hummus, tahini, comment, status);


            Intent intentToOpenService = new Intent(ActivityNewOrder.this, ActivityEditOrder.class);
            intentToOpenService.putExtra("ID", id);
            intentToOpenService.putExtra("customer_name", customer_name);
            intentToOpenService.putExtra("pickles", pickles);
            intentToOpenService.putExtra("hummus", hummus);
            intentToOpenService.putExtra("tahini", tahini);
            intentToOpenService.putExtra("comment", comment);
            intentToOpenService.putExtra("status", status);
            startActivity(intentToOpenService);
            finish();


        });


    }

    public static boolean isNumeric(String strNum) {
        double d;
        if (strNum == null) {
            return false;
        }
        try {
            d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return d >= 0 && d <= 10;

    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {


        super.onSaveInstanceState(outState);

        outState.putString("customer_name", customer_name);
        outState.putInt("pickles", pickles);

        outState.putBoolean("hummus",hummus);
        outState.putBoolean("tahini",tahini);

        outState.putString("comment",comment);
        outState.putString("status",status);



    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        customer_name = savedInstanceState.getString("customer_name");
        pickles = savedInstanceState.getInt("pickles");
        hummus = savedInstanceState.getBoolean("hummus");
        tahini = savedInstanceState.getBoolean("tahini");
        comment = savedInstanceState.getString("comment");
        status = savedInstanceState.getString("saveState");
    }
}
