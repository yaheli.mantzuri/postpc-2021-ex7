package com.example.postpc2021ex7;

import org.junit.runner.RunWith;

import android.app.Application;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.FirebaseFirestore;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.eq;

@RunWith(RobolectricTestRunner.class)
@Config(sdk = 28, application = Application.class)
public class OrderTest extends TestCase {




    private ActivityController<MainActivity> activityController;
    private ActivityController<ActivityEditOrder> editActivity;
    private ActivityController<ActivityNewOrder> newActivity;
    private HolderNew mockHolder;

    @Before
    public void setup() {
        mockHolder = Mockito.mock(HolderNew.class);
        activityController = Robolectric.buildActivity(MainActivity.class);
        editActivity = Robolectric.buildActivity(ActivityEditOrder.class);
        newActivity = Robolectric.buildActivity(ActivityNewOrder.class);
        ActivityNewOrder activityUnderTest = newActivity.get();
        activityUnderTest.holder = mockHolder;
    }

    @Test
    public void test_1() {
        newActivity.create().visible();
        ActivityNewOrder activityNewOrderTest = newActivity.get();
        EditText comment = activityNewOrderTest.findViewById(R.id.edit_comment);
        String userInput = comment.getText().toString();
        assertTrue(userInput.isEmpty());
    }

    @Test
    public void test_2() {
        newActivity.create().visible();
        ActivityNewOrder activityNewOrderTest = newActivity.get();
        EditText name = activityNewOrderTest.findViewById(R.id.edit_name);
        String userInput = name.getText().toString();
        assertTrue(userInput.isEmpty());
    }


    @Test
    public void test_3() {
        newActivity.create().visible();
        ActivityNewOrder activityNewOrderTest = newActivity.get();
        EditText p = activityNewOrderTest.findViewById(R.id.edit_pickles);
        Button b = activityNewOrderTest.findViewById(R.id.button_confirm);
        p.setText("11");
        assertFalse(b.isEnabled());
    }


    @Test
    public void test_4() {
        newActivity.create().visible();
        ActivityNewOrder activityNewOrderTest = newActivity.get();
        EditText p = activityNewOrderTest.findViewById(R.id.edit_pickles);
        Button b = activityNewOrderTest.findViewById(R.id.button_confirm);
        p.setText("5");
        assertTrue(b.isEnabled());
    }

}
